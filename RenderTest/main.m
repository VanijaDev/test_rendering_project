//
//  main.m
//  RenderTest
//
//  Created by Ivan Solomichev Vakoms on 9/17/14.
//  Copyright (c) 2014 VanijaDev. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[])
{
    return NSApplicationMain(argc, argv);
}
