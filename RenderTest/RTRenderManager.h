//
//  RTRenderManager.h
//  RenderTest
//
//  Created by Ivan Solomichev Vakoms on 9/19/14.
//  Copyright (c) 2014 VanijaDev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RTRenderManager : NSObject

- (void)beginDrawingWithContextSize:(NSSize)aContextSize
                       drawingBlock:(void(^)(void))aDrawingBlock
                    completionBlock:(void(^)(NSBitmapImageRep *resultRep))aCompletionBlock;

- (void)saveBitmapRepresentation:(NSBitmapImageRep *)anImageRep
                        withType:(NSBitmapImageFileType)aImageType
                             dpi:(CGFloat)aDpiValue
                         quality:(CGFloat)aQuality
                          atPath:(NSString *)aFilePath;

@end
