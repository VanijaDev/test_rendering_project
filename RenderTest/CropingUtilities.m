//
//  CropingUtilities.m
//  Smart Albums
//
//  Created by Ivan Podibka on 9/17/13.
//
//

#import "CropingUtilities.h"

@implementation CropingUtilities

+ (NSImage *)rotatedImage:(NSImage *)anOriginalImage
                withAngle:(CGFloat)anAngle {
    if (anAngle == 0 || !anOriginalImage) {
        return anOriginalImage;
    }
    
    NSSize originalImageSize = [self realSizeOfImage:anOriginalImage];
    
//    CGFloat displayScale = [Utilities getDisplayScale];
//    if (displayScale > 1.0) {
//        // Fix perfomance problem on Retina Mac Book.
//        originalImageSize.width = originalImageSize.width/displayScale;
//        originalImageSize.height = originalImageSize.height/displayScale;
//    }
    
    // Calculate the bounds for the rotated image
    // We do this by affine-transforming the bounds rectangle
    NSRect imageBounds = {NSZeroPoint, originalImageSize};
    
    NSRect rotatedBounds = CGRectApplyAffineTransform(imageBounds, CGAffineTransformMakeRotation(anAngle));
    
    rotatedBounds.origin = NSZeroPoint;
    NSImage* rotatedImage = [[NSImage alloc] initWithSize:rotatedBounds.size];
    // Center the image within the rotated bounds
    imageBounds.origin.x = NSMidX(rotatedBounds) - (NSWidth(imageBounds) / 2);
    imageBounds.origin.y = NSMidY(rotatedBounds) - (NSHeight(imageBounds) / 2);
    
    // Start a new transform, to transform the image
    NSAffineTransform *transform = [NSAffineTransform transform];
    
    if (anAngle != 0) {
        // Move coordinate system to the center
        // (since we want to rotate around the center)
        [transform translateXBy:+(NSWidth(rotatedBounds) / 2)
                            yBy:+(NSHeight(rotatedBounds) / 2)];
        [transform rotateByRadians:anAngle];
        [transform translateXBy:-(NSWidth(rotatedBounds) / 2)
                            yBy:-(NSHeight(rotatedBounds) / 2)];
        
    }
    
    
    // Draw the original image, rotated, into the new image
    // Note: This "drawing" is done off-screen.
    [rotatedImage lockFocus];
    [transform concat];
    [anOriginalImage drawInRect:imageBounds fromRect:NSZeroRect operation:NSCompositeCopy fraction:1.0f];
    [rotatedImage unlockFocus];
    
    return rotatedImage;
}

+ (NSRect)rotatedFrame:(NSRect)aFrame withAngle:(CGFloat)anAngle rotationPoint:(NSPoint)aRotationPoint {
    NSAffineTransform *transform = [NSAffineTransform transform];
    NSBezierPath *boundsPath = [NSBezierPath bezierPathWithRect:aFrame];
    [transform translateXBy:aRotationPoint.x
                        yBy:aRotationPoint.y];
    [transform rotateByRadians:anAngle];
    [transform translateXBy:-aRotationPoint.x
                        yBy:-aRotationPoint.y];
    [boundsPath transformUsingAffineTransform:transform];
    return [boundsPath bounds];
}





+ (NSSize)realSizeOfImageData:(NSData *)anImageData {
    NSBitmapImageRep *originalImageRep = [NSBitmapImageRep imageRepWithData:anImageData];
    NSSize originalSize = NSMakeSize(originalImageRep.pixelsWide, originalImageRep.pixelsHigh);
    
    originalImageRep = nil;
    
    return originalSize;
}

+ (NSSize)realSizeOfImage:(NSImage *)anImage {
    NSData *data = [anImage TIFFRepresentation];
    return [self realSizeOfImageData:data];
}

+ (NSRect)roudedRect:(NSRect)aRect {
    
    aRect.origin.x = lroundf(aRect.origin.x);
    aRect.origin.y = lroundf(aRect.origin.y);
    aRect.size.width = lroundf(aRect.size.width);
    aRect.size.height = lroundf(aRect.size.height);
    
    return aRect;
}

+ (NSSize)exportedSize:(NSSize)aSize
               withDpi:(NSInteger)aDpi {
    return NSMakeSize(aSize.width*aDpi, aSize.height*aDpi);
}
@end
