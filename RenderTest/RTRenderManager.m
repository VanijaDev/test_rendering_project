//
//  RTRenderManager.m
//  RenderTest
//
//  Created by Ivan Solomichev Vakoms on 9/19/14.
//  Copyright (c) 2014 VanijaDev. All rights reserved.
//

#import "RTRenderManager.h"

@implementation RTRenderManager

- (void)beginDrawingWithContextSize:(NSSize)aContextSize
                       drawingBlock:(void(^)(void))aDrawingBlock
                    completionBlock:(void(^)(NSBitmapImageRep *resultRep))aCompletionBlock {
    // create a representation with correct color space
    NSRect offscreenRect = NSZeroRect;
    offscreenRect.origin = NSZeroPoint;
    offscreenRect.size = aContextSize;
    
    NSBitmapImageRep *rep = [[NSBitmapImageRep alloc] initWithBitmapDataPlanes:nil
                                                                    pixelsWide:offscreenRect.size.width
                                                                    pixelsHigh:offscreenRect.size.height\
                                                                 bitsPerSample:8
                                                               samplesPerPixel:4
                                                                      hasAlpha:YES
                                                                      isPlanar:NO
                                                                colorSpaceName:NSCalibratedRGBColorSpace
                                                                  bitmapFormat:0
                                                                   bytesPerRow:(4 * offscreenRect.size.width)
                                                                  bitsPerPixel:32];
    // set the default color profile data
    //    [rep setProperty:NSImageColorSyncProfileData withValue:[self.colorProfile ICCProfileData]];
    
    [NSGraphicsContext saveGraphicsState];
    [NSGraphicsContext setCurrentContext:[NSGraphicsContext graphicsContextWithBitmapImageRep:rep]];
    
    // drawin content
    if (aDrawingBlock != NULL) {
        aDrawingBlock();
    }
    
    [NSGraphicsContext restoreGraphicsState];
    
    // send a result
    if (aCompletionBlock != NULL) {
        aCompletionBlock(rep);
    }
}

- (void)saveBitmapRepresentation:(NSBitmapImageRep *)anImageRep
                        withType:(NSBitmapImageFileType)aImageType
                             dpi:(CGFloat)aDpiValue
                         quality:(CGFloat)aQuality
                          atPath:(NSString *)aFilePath {
    @autoreleasepool {
        NSSize pointsSize = anImageRep.size;
		NSSize pixelSize = NSMakeSize(anImageRep.pixelsWide, anImageRep.pixelsHigh);
		
		NSSize updatedPointsSize = pointsSize;
		
        // set output dpi
		updatedPointsSize.width = (72.0f * pixelSize.width)/aDpiValue;
		updatedPointsSize.height = (72.0f * pixelSize.height)/aDpiValue;
		
		[anImageRep setSize:updatedPointsSize];
        
        // create a data for a specific image type and set the compration rate of output image
		NSData *data = [anImageRep representationUsingType:aImageType
                                                properties:@{NSImageCompressionFactor : [NSNumber numberWithFloat:aQuality]}];
        
		[data writeToFile:aFilePath atomically:YES];
		
		data = nil;
		anImageRep = nil;
    }
}

@end
