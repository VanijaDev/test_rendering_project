//
//  RTAppDelegate.h
//  RenderTest
//
//  Created by Ivan Solomichev Vakoms on 9/17/14.
//  Copyright (c) 2014 VanijaDev. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface RTAppDelegate : NSObject <NSApplicationDelegate>

@property (assign) IBOutlet NSWindow *window;

@end
