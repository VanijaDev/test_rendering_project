//
//  RTImagesRender.m
//  RenderTest
//
//  Created by Oleg Sehelin on 9/19/14.
//  Copyright (c) 2014 VanijaDev. All rights reserved.
//

#import "RTImagesRender.h"
#import "CropingUtilities.h"

@implementation RTImagesRender

- (void)drawImagesForSpreadWithImageName:(NSString*)anImageName dpi:(NSInteger)aDPI {
    NSArray *framesArray = [NSArray arrayWithObjects:
                            [NSValue valueWithRect:(NSRect){158.0,476.0f,306.0f,230.0f}],
                            [NSValue valueWithRect:(NSRect){525.0,476.0f,306.0f,230.0f}],
                            [NSValue valueWithRect:(NSRect){158.0,158.0f,306.0f,230.0f}],
                            [NSValue valueWithRect:(NSRect){555.0,158.0f,306.0f,230.0f}],
                            [NSValue valueWithRect:(NSRect){994.0,409.0f,130.0f,98.0f}],
                            [NSValue valueWithRect:(NSRect){1150.0,409.0f,130.0f,98.0f}],
                            [NSValue valueWithRect:(NSRect){994.0,265.0f,130.0f,98.0f}],
                            [NSValue valueWithRect:(NSRect){1170.0,265.0f,130.0f,98.0f}],
                            nil];
    
    __block NSArray *anglesArray = [NSArray arrayWithObjects:
                                    [NSNumber numberWithFloat:0.0f],
                                    [NSNumber numberWithFloat:1.0f],
                                    [NSNumber numberWithFloat:3.0f],
                                    [NSNumber numberWithFloat:45.0f],
                                    [NSNumber numberWithFloat:0.0f],
                                    [NSNumber numberWithFloat:1.0f],
                                    [NSNumber numberWithFloat:3.0f],
                                    [NSNumber numberWithFloat:45.0f],
                                    nil];
    
    
    [framesArray enumerateObjectsUsingBlock:^(NSValue *frame, NSUInteger idx, BOOL *stop) {
        NSRect startFrame = frame.rectValue;
        NSImage *originalImage = [NSImage imageNamed:anImageName];
        startFrame.origin.x *=aDPI/72;
        startFrame.origin.y *=aDPI/72;
        startFrame.size.height *=aDPI/72;
        startFrame.size.width = startFrame.size.height*(originalImage.size.width/originalImage.size.height);
        
        NSImage *finishImage = [self drawImageWithName:anImageName frame:(NSRect){0.0f,0.0f,startFrame.size.width,startFrame.size.height} rotationAngle:[[anglesArray objectAtIndex:idx] floatValue]];
        
        startFrame = [CropingUtilities rotatedFrame:startFrame withAngle:[[anglesArray objectAtIndex:idx] floatValue] * M_PI / 180.0f rotationPoint:NSMakePoint(startFrame.origin.x + startFrame.size.width/2, startFrame.origin.y + startFrame.size.height/2)];
        
        startFrame = [CropingUtilities roudedRect:startFrame];
        
        [finishImage drawInRect:startFrame
                       fromRect:NSZeroRect
                      operation:NSCompositeSourceOver
                       fraction:1.0f
                 respectFlipped:NO
                          hints:nil];
        
        finishImage = nil;
    }];
}

- (void)drawFirstPageForDpi:(NSInteger)aDPI {
    NSArray *framesArray = [NSArray arrayWithObjects:
                            [NSValue valueWithRect:(NSRect){9.0,107.0f,374.0f,280.0f}],
                            [NSValue valueWithRect:(NSRect){400.0,259.0f,171.0f,128.0f}],
                            [NSValue valueWithRect:(NSRect){591.0,345.0f,56.0f,42.0f}],
                            nil];
    
    [framesArray enumerateObjectsUsingBlock:^(NSValue *frame, NSUInteger idx, BOOL *stop) {
        NSRect newRect = frame.rectValue;
        newRect.origin.x *= aDPI/72;
        newRect.origin.y *= aDPI/72;
        newRect.size.width *= aDPI/72;
        newRect.size.height *= aDPI/72;
        
        NSImage *finishImage = [self drawImageWithName:@"star-chart-bars-full-600dpi.png" frame:(NSRect){0.0f,0.0f,newRect.size.width,newRect.size.height} rotationAngle:0];
    
        [finishImage drawInRect:newRect
                       fromRect:NSZeroRect
                      operation:NSCompositeSourceOver
                       fraction:1.0f
                 respectFlipped:NO
                          hints:nil];
        
        finishImage = nil;
    }];
}


- (NSImage*)drawImageWithName:(NSString*)anImageName
                        frame:(NSRect)aFrame
                rotationAngle:(CGFloat)angle {
    
    NSImage *resultImage = [[NSImage alloc] initWithSize:NSMakeSize(NSWidth(aFrame), NSHeight(aFrame))];
    
    [resultImage lockFocus];
    
    NSImage *originImage = [CropingUtilities rotatedImage:[NSImage imageNamed:anImageName] withAngle:angle * M_PI/ 180.0f];
    
    [originImage drawInRect:aFrame
                   fromRect:NSZeroRect
                  operation:NSCompositeSourceOver
                   fraction:1.0f
             respectFlipped:NO
                      hints:nil];
    
    [resultImage unlockFocus];
    
    return resultImage;
}


//  text
- (void)drawImagesForSpreadWithText:(NSString *)aString {
    NSArray *framesArray = [NSArray arrayWithObjects:
                            [NSValue valueWithRect:(NSRect){158.0,476.0f,550.0f,20.0f}],
                            [NSValue valueWithRect:(NSRect){158.0,456.0f,550.0f,20.0f}],
                            [NSValue valueWithRect:(NSRect){158.0,436.0f,550.0f,20.0f}],
                            
                            [NSValue valueWithRect:(NSRect){525.0,476.0f,550.0f,20.0f}],
                            [NSValue valueWithRect:(NSRect){525.0,456.0f,550.0f,20.0f}],
                            [NSValue valueWithRect:(NSRect){525.0,436.0f,550.0f,20.0f}],
                            
                            [NSValue valueWithRect:(NSRect){158.0,286.0f,550.0f,20.0f}],
                            [NSValue valueWithRect:(NSRect){158.0,266.0f,550.0f,20.0f}],
                            [NSValue valueWithRect:(NSRect){158.0,246.0f,550.0f,20.0f}],
                            
                            [NSValue valueWithRect:(NSRect){525.0,156.0f,550.0f,20.0f}],
                            [NSValue valueWithRect:(NSRect){525.0,136.0f,550.0f,20.0f}],
                            [NSValue valueWithRect:(NSRect){525.0,116.0f,550.0f,20.0f}],
                            
                            nil];
    
    __block NSArray *anglesArray = [NSArray arrayWithObjects:
                                    [NSNumber numberWithFloat:0.0f],
                                    [NSNumber numberWithFloat:0.0f],
                                    [NSNumber numberWithFloat:0.0f],
                                    
                                    [NSNumber numberWithFloat:1.0f],
                                    [NSNumber numberWithFloat:1.0f],
                                    [NSNumber numberWithFloat:1.0f],
                                    
                                    [NSNumber numberWithFloat:3.0f],
                                    [NSNumber numberWithFloat:3.0f],
                                    [NSNumber numberWithFloat:3.0f],
                                    
                                    [NSNumber numberWithFloat:45.0f],
                                    [NSNumber numberWithFloat:45.0f],
                                    [NSNumber numberWithFloat:45.0f],
                                    nil];
    __block CGFloat fontSize = roundf(8.0f * 4.16);
    __block CGRect imageFrame;
    
    [framesArray enumerateObjectsUsingBlock:^(NSValue *frame, NSUInteger idx, BOOL *stop) {
        imageFrame = frame.rectValue;
        imageFrame.origin.x *= 4.16;
        imageFrame.origin.y *= 4.16;
        imageFrame.size.width *= 4.16;
        imageFrame.size.height *= 4.16;
        NSImage *resultImage = [[NSImage alloc] initWithSize:NSMakeSize(NSWidth(imageFrame), NSHeight(imageFrame))];
        [resultImage lockFocus];
        
        if (idx % 6 == 0)
            fontSize += roundf(4.0f * 4.16);
        
        //  draw image
        NSMutableAttributedString* textStr = [[NSMutableAttributedString alloc] initWithString:aString];
        if (idx == 3 || idx == 4 || idx == 5 || idx == 9 || idx == 10 || idx == 11)
            textStr = [[NSMutableAttributedString alloc] initWithString:@"THE QUICK BROWN FOX JUMPS OVER THE LAZY DOG"];
        
        if (idx == 1 || idx == 4 || idx == 7 || idx == 10)
            [textStr addAttribute:NSKernAttributeName value:@4 range:NSMakeRange(0, textStr.length)];
        
        if (idx == 2 || idx == 5 || idx == 8 || idx == 11)
            [textStr addAttribute:NSKernAttributeName value:@10 range:NSMakeRange(0, textStr.length)];
        
        [textStr addAttribute:NSFontAttributeName value:[NSFont fontWithName:@"Times New Roman" size:fontSize] range:NSMakeRange(0, textStr.length)];
        [textStr addAttribute:NSForegroundColorAttributeName value:[NSColor blackColor] range:NSMakeRange(0, textStr.length)];
        [textStr addAttribute:NSFontAttributeName value:[NSFont fontWithName:@"Times New Roman Bold Italic" size:fontSize] range:NSMakeRange(26, textStr.length - 26)];
        
        [textStr drawInRect:(NSRect){0.0,0.0,imageFrame.size.width,imageFrame.size.height}];
        
        [resultImage unlockFocus];
        
        NSImage *textImage = [CropingUtilities rotatedImage:resultImage withAngle:[[anglesArray objectAtIndex:idx] floatValue] * M_PI/ 180.0f];
        
        NSRect startFrame = imageFrame;
        startFrame.size.width = textImage.size.width;
        startFrame.size.height = textImage.size.height;
        
        
        startFrame = [CropingUtilities rotatedFrame:startFrame withAngle:[[anglesArray objectAtIndex:idx] floatValue] * M_PI / 180.0f rotationPoint:NSMakePoint(startFrame.origin.x + startFrame.size.width/2, startFrame.origin.y + startFrame.size.height/2)];
        startFrame.size.width = textImage.size.width;
        startFrame.size.height = textImage.size.height;
        
        startFrame = [CropingUtilities roudedRect:startFrame];
        
        [textImage drawInRect:startFrame
                     fromRect:NSZeroRect
                    operation:NSCompositeSourceOver
                     fraction:1.0f
               respectFlipped:NO
                        hints:nil];
    }];
}

@end
