//
//  RTImagesRender.h
//  RenderTest
//
//  Created by Oleg Sehelin on 9/19/14.
//  Copyright (c) 2014 VanijaDev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RTImagesRender : NSObject

- (void)drawImagesForSpreadWithImageName:(NSString*)anImageName dpi:(NSInteger)aDPI;
- (void)drawFirstPageForDpi:(NSInteger)aDPI;


- (void)drawImagesForSpreadWithText:(NSString* )aString;
@end
