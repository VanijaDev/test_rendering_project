//
//  RTTextRender.m
//  RenderTest
//
//  Created by Ivan Solomichev Vakoms on 9/19/14.
//  Copyright (c) 2014 VanijaDev. All rights reserved.
//

#import "RTTextRender.h"
#import "CropingUtilities.h"


static float canvasWidth = 1440.0f;
static float canvasHeight = 720.0f;

@implementation RTTextRender

- (void)drawImagesForSpreadWithText:(NSString *)aString dpi:(NSInteger)aDPI{
    NSArray *framesArray = [NSArray arrayWithObjects:
                            //  1
                            [NSValue valueWithRect:(NSRect){163.0,canvasHeight - 19.0f,230.0f,14.0f}],
                            [NSValue valueWithRect:(NSRect){163.0,canvasHeight - 38.0f,450.0f,14.0f}],
                            [NSValue valueWithRect:(NSRect){163.0,canvasHeight - 60.0f,450.0f,14.0f}],

                            [NSValue valueWithRect:(NSRect){163.0,canvasHeight - 101.0f,450.0f,14.0f}],
                            [NSValue valueWithRect:(NSRect){163.0,canvasHeight - 121.0f,450.0f,14.0f}],
                            [NSValue valueWithRect:(NSRect){163.0,canvasHeight - 141.0f,450.0f,14.0f}],
                            
                            [NSValue valueWithRect:(NSRect){163.0,canvasHeight - 190.0f,450.0f,14.0f}],
                            [NSValue valueWithRect:(NSRect){163.0,canvasHeight - 211.0f,450.0f,14.0f}],
                            [NSValue valueWithRect:(NSRect){163.0,canvasHeight - 231.0f,450.0f,14.0f}],
                            
                            [NSValue valueWithRect:(NSRect){163.0,canvasHeight - 272.0f,450.0f,14.0f}],
                            [NSValue valueWithRect:(NSRect){163.0,canvasHeight - 292.0f,450.0f,14.0f}],
                            [NSValue valueWithRect:(NSRect){163.0,canvasHeight - 312.0f,450.0f,14.0f}],
                            
                            //  2
                            [NSValue valueWithRect:(NSRect){156.0,canvasHeight - 387.0f,230.0f,14.0f}],
                            [NSValue valueWithRect:(NSRect){157.0,canvasHeight - 407.0f,450.0f,14.0f}],
                            [NSValue valueWithRect:(NSRect){158.0,canvasHeight - 428.0f,450.0f,14.0f}],
                            
                            [NSValue valueWithRect:(NSRect){162.0,canvasHeight - 469.0f,450.0f,14.0f}],
                            [NSValue valueWithRect:(NSRect){163.0,canvasHeight - 489.0f,450.0f,14.0f}],
                            [NSValue valueWithRect:(NSRect){164.0,canvasHeight - 510.0f,450.0f,14.0f}],
                            
                            [NSValue valueWithRect:(NSRect){166.0,canvasHeight - 558.0f,450.0f,14.0f}],
                            [NSValue valueWithRect:(NSRect){167.0,canvasHeight - 578.0f,450.0f,14.0f}],
                            [NSValue valueWithRect:(NSRect){168.0,canvasHeight - 599.0f,450.0f,14.0f}],
                            
                            [NSValue valueWithRect:(NSRect){172.0,canvasHeight - 640.0f,450.0f,14.0f}],
                            [NSValue valueWithRect:(NSRect){173.0,canvasHeight - 661.0f,450.0f,14.0f}],
                            [NSValue valueWithRect:(NSRect){174.0,canvasHeight - 681.0f,450.0f,14.0f}],
                            
                            //  3
                            [NSValue valueWithRect:(NSRect){589.0,canvasHeight - 30.0f,230.0f,14.0f}],
                            [NSValue valueWithRect:(NSRect){589.0,canvasHeight - 49.0f,450.0f,14.0f}],
                            [NSValue valueWithRect:(NSRect){590.0,canvasHeight - 71.0f,450.0f,14.0f}],
                            
                            [NSValue valueWithRect:(NSRect){592.0,canvasHeight - 112.0f,450.0f,14.0f}],
                            [NSValue valueWithRect:(NSRect){592.0,canvasHeight - 132.0f,450.0f,14.0f}],
                            [NSValue valueWithRect:(NSRect){593.0,canvasHeight - 153.0f,450.0f,14.0f}],
                            
                            [NSValue valueWithRect:(NSRect){593.0,canvasHeight - 201.0f,450.0f,14.0f}],
                            [NSValue valueWithRect:(NSRect){594.0,canvasHeight - 221.0f,450.0f,14.0f}],
                            [NSValue valueWithRect:(NSRect){594.0,canvasHeight - 242.0f,450.0f,14.0f}],
                            
                            [NSValue valueWithRect:(NSRect){596.0,canvasHeight - 283.0f,450.0f,14.0f}],
                            [NSValue valueWithRect:(NSRect){596.0,canvasHeight - 304.0f,450.0f,14.0f}],
                            [NSValue valueWithRect:(NSRect){597.0,canvasHeight - 324.0f,450.0f,14.0f}],
                            
                            //  4
                            //  configured manually
                            [NSValue valueWithRect:(NSRect){934.0,canvasHeight - 456.0f,230.0f,14.0f}],
                            [NSValue valueWithRect:(NSRect){981.0,canvasHeight - 440.0f,450.0f,14.0f}],
                            [NSValue valueWithRect:(NSRect){996.0,canvasHeight - 456.0f,450.0f,14.0f}],
                            
                            [NSValue valueWithRect:(NSRect){1026.0,canvasHeight - 483.0f,450.0f,14.0f}],
                            [NSValue valueWithRect:(NSRect){1041.0,canvasHeight - 498.0f,450.0f,14.0f}],
                            [NSValue valueWithRect:(NSRect){1055.0,canvasHeight - 512.0f,450.0f,14.0f}],
                            
                            [NSValue valueWithRect:(NSRect){1089.0,canvasHeight - 547.0f,450.0f,14.0f}],
                            [NSValue valueWithRect:(NSRect){1103.0,canvasHeight - 561.0f,450.0f,14.0f}],
                            [NSValue valueWithRect:(NSRect){1118.0,canvasHeight - 576.0f,450.0f,14.0f}],
                            
                            [NSValue valueWithRect:(NSRect){1148.0,canvasHeight - 604.0f,450.0f,14.0f}],
                            [NSValue valueWithRect:(NSRect){1163.0,canvasHeight - 618.0f,450.0f,14.0f}],
                            [NSValue valueWithRect:(NSRect){1177.0,canvasHeight - 633.0f,450.0f,14.0f}],
                            
                            nil];
    
    __block CGRect imageFrame;
    
    [framesArray enumerateObjectsUsingBlock:^(NSValue *frame, NSUInteger idx, BOOL *stop) {
        imageFrame = frame.rectValue;
        imageFrame.origin.x *= aDPI/72;
        imageFrame.origin.y *= aDPI/72;
        imageFrame.size.width *= aDPI/72;
        imageFrame.size.height *= aDPI/72;
        NSImage *resultImage = [[NSImage alloc] initWithSize:NSMakeSize(NSWidth(imageFrame), NSHeight(imageFrame))];
        [resultImage lockFocus];
        
        //  font
        CGFloat fontSize = roundf(8.0f *  aDPI/72);
        if ( (idx > 5 && idx < 12) || (idx > 17 && idx < 24) || (idx > 29 && idx < 36) || (idx > 41 && idx < 48)) {
            
            fontSize = roundf(12.0f *  aDPI/72);
        }
        
        //  draw image
        NSMutableAttributedString* textStr = [[NSMutableAttributedString alloc] initWithString:aString];
        if (idx == 3 || idx == 4 || idx == 5 || idx == 9 || idx == 10 || idx == 11 || idx == 15 || idx == 16 || idx == 17 || idx == 21 || idx == 22 || idx == 23 || idx == 27 || idx == 28 || idx == 29 || idx == 33 || idx == 34 || idx == 35 ||idx == 39 || idx == 40  || idx == 41 || idx == 45 || idx == 46 || idx == 47)
            textStr = [[NSMutableAttributedString alloc] initWithString:@"THE QUICK BROWN FOX JUMPS OVER THE LAZY DOG"];
        
        //  trqcking
        if (idx == 1 || idx == 4 || idx == 7 || idx == 10 || idx == 13 || idx == 16 || idx == 19 || idx == 22 || idx == 25 || idx == 28 || idx == 31 || idx == 34 || idx == 37 || idx == 40 || idx == 43 || idx == 46)
            [textStr addAttribute:NSKernAttributeName value:@(4 * 0.416) range:NSMakeRange(0, textStr.length)];
        else if (idx == 2 || idx == 5 || idx == 8 || idx == 11 || idx == 14 || idx == 17 || idx == 20 || idx == 23 || idx == 26 || idx == 29 || idx == 32 || idx == 35 || idx == 38 || idx == 41 || idx == 44 || idx == 47)
            [textStr addAttribute:NSKernAttributeName value:@(10 * 0.416) range:NSMakeRange(0, textStr.length)];
        
        [textStr addAttribute:NSFontAttributeName value:[NSFont fontWithName:@"Times New Roman" size:fontSize] range:NSMakeRange(0, textStr.length)];
        [textStr addAttribute:NSForegroundColorAttributeName value:[NSColor blackColor] range:NSMakeRange(0, textStr.length)];
        [textStr addAttribute:NSFontAttributeName value:[NSFont fontWithName:@"Times New Roman Bold Italic" size:fontSize] range:NSMakeRange(26, textStr.length - 26)];
        
        [textStr drawInRect:(NSRect){0.0,0.0,imageFrame.size.width,imageFrame.size.height}];
        
        [resultImage unlockFocus];
        
        //  angle
        float angleRotate = 0.0f;
        if (idx > 35)
            angleRotate = 45;
        else if (idx > 23)
            angleRotate = 1.0;
        else if (idx > 11)
            angleRotate = 3.0f;
        
        NSImage *textImage = [CropingUtilities rotatedImage:resultImage withAngle:angleRotate * M_PI/ 180.0f];
        
        NSRect startFrame = imageFrame;
        startFrame.size.width = textImage.size.width;
        startFrame.size.height = textImage.size.height;
        
        startFrame = [CropingUtilities rotatedFrame:startFrame withAngle:angleRotate * M_PI / 180.0f rotationPoint:NSMakePoint(startFrame.origin.x + startFrame.size.width/2, startFrame.origin.y + startFrame.size.height/2)];
        startFrame.size.width = textImage.size.width;
        startFrame.size.height = textImage.size.height;
        
        startFrame = [CropingUtilities roudedRect:startFrame];
        
        [textImage drawInRect:startFrame
                     fromRect:NSZeroRect
                    operation:NSCompositeSourceOver
                     fraction:1.0f
               respectFlipped:NO
                        hints:nil];
    }];
}

- (void)drawImagesForFirstSpreadWithText:(NSString *)aString dpi:(NSInteger)aDPI{
    NSArray *framesArray = [NSArray arrayWithObjects:
                            //  1
                            [NSValue valueWithRect:(NSRect){9.0,canvasHeight - 19.0f,230.0f,15.0f}],
                            [NSValue valueWithRect:(NSRect){9.0,canvasHeight - 38.0f,450.0f,15.0f}],
                            [NSValue valueWithRect:(NSRect){9.0,canvasHeight - 60.0f,450.0f,15.0f}],
                            
                            [NSValue valueWithRect:(NSRect){9.0,canvasHeight - 101.0f,450.0f,15.0f}],
                            [NSValue valueWithRect:(NSRect){9.0,canvasHeight - 121.0f,450.0f,15.0f}],
                            [NSValue valueWithRect:(NSRect){9.0,canvasHeight - 141.0f,450.0f,15.0f}],
                            
                            [NSValue valueWithRect:(NSRect){9.0,canvasHeight - 190.0f,450.0f,15.0f}],
                            [NSValue valueWithRect:(NSRect){9.0,canvasHeight - 211.0f,450.0f,15.0f}],
                            [NSValue valueWithRect:(NSRect){9.0,canvasHeight - 231.0f,450.0f,15.0f}],
                            
                            [NSValue valueWithRect:(NSRect){9.0,canvasHeight - 272.0f,450.0f,15.0f}],
                            [NSValue valueWithRect:(NSRect){9.0,canvasHeight - 292.0f,450.0f,15.0f}],
                            [NSValue valueWithRect:(NSRect){9.0,canvasHeight - 312.0f,550.0f,15.0f}],
                            nil];
    
    __block CGRect imageFrame;
    
    [framesArray enumerateObjectsUsingBlock:^(NSValue *frame, NSUInteger idx, BOOL *stop) {
        imageFrame = frame.rectValue;
        imageFrame.origin.x *=  aDPI/72;
        imageFrame.origin.y *=  aDPI/72;
        imageFrame.size.width *=  aDPI/72;
        imageFrame.size.height *=  aDPI/72;
        NSImage *resultImage = [[NSImage alloc] initWithSize:NSMakeSize(NSWidth(imageFrame), NSHeight(imageFrame))];
        [resultImage lockFocus];
        
        //  font
        CGFloat fontSize = roundf(8.0f *  aDPI/72);
        if (idx > 5) {
            fontSize = roundf(12.0f *  aDPI/72);
        }
        
        //  draw image
        NSMutableAttributedString* textStr = [[NSMutableAttributedString alloc] initWithString:aString];
        if (idx == 3 || idx == 4 || idx == 5 || idx == 9 || idx == 10 || idx == 11)
            textStr = [[NSMutableAttributedString alloc] initWithString:@"THE QUICK BROWN FOX JUMPS OVER THE LAZY DOG"];
        
        //  trqcking
        if (idx == 1 || idx == 4 || idx == 7 || idx == 10)
            [textStr addAttribute:NSKernAttributeName value:@(4 * 0.416) range:NSMakeRange(0, textStr.length)];
        else if (idx == 2 || idx == 5 || idx == 8 || idx == 11)
            [textStr addAttribute:NSKernAttributeName value:@(10 * 0.416) range:NSMakeRange(0, textStr.length)];
        
        [textStr addAttribute:NSFontAttributeName value:[NSFont fontWithName:@"Times New Roman" size:fontSize] range:NSMakeRange(0, textStr.length)];
        [textStr addAttribute:NSForegroundColorAttributeName value:[NSColor blackColor] range:NSMakeRange(0, textStr.length)];
        [textStr addAttribute:NSFontAttributeName value:[NSFont fontWithName:@"Times New Roman Bold Italic" size:fontSize] range:NSMakeRange(26, textStr.length - 26)];
        
        [textStr drawInRect:(NSRect){0.0,0.0,imageFrame.size.width,imageFrame.size.height}];
        
        [resultImage unlockFocus];
        
        //  angle
        float angleRotate = 0.0f;
        
        NSImage *textImage = [CropingUtilities rotatedImage:resultImage withAngle:angleRotate * M_PI/ 180.0f];
        
        NSRect startFrame = imageFrame;
        startFrame.size.width = textImage.size.width;
        startFrame.size.height = textImage.size.height;
        
        startFrame = [CropingUtilities rotatedFrame:startFrame withAngle:angleRotate * M_PI / 180.0f rotationPoint:NSMakePoint(startFrame.origin.x + startFrame.size.width/2, startFrame.origin.y + startFrame.size.height/2)];
        startFrame.size.width = textImage.size.width;
        startFrame.size.height = textImage.size.height;
        
        startFrame = [CropingUtilities roudedRect:startFrame];
        
        NSRect finishRect = frame.rectValue;
        finishRect.origin.x *= aDPI/72;
        finishRect.origin.y *= aDPI/72;
        finishRect.size.width *= aDPI/72;
        finishRect.size.height *= aDPI/72;

        
        [textImage drawInRect:finishRect
                     fromRect:NSZeroRect
                    operation:NSCompositeSourceOver
                     fraction:1.0f
               respectFlipped:YES
                        hints:nil];
    }];
}

@end
