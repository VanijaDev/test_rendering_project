//
//  RTAppDelegate.m
//  RenderTest
//
//  Created by Ivan Solomichev Vakoms on 9/17/14.
//  Copyright (c) 2014 VanijaDev. All rights reserved.
//

#import "RTAppDelegate.h"
#import "RTRenderManager.h"
#import "RTImagesRender.h"
#import "RTTextRender.h"
#import "CropingUtilities.h"

#define DPI 300

@interface RTAppDelegate ()


@end

@implementation RTAppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    
    //export size
    NSSize exportSize = NSMakeSize(20, 10);
    
    __block RTImagesRender *imagesRender = [RTImagesRender new];
    __block RTTextRender *textRender = [RTTextRender new];
    
    RTRenderManager *renderManager = [RTRenderManager new];
    
    [renderManager beginDrawingWithContextSize:[CropingUtilities exportedSize:exportSize withDpi:DPI] drawingBlock:^{
        [imagesRender drawImagesForSpreadWithImageName:@"star-chart-bars-full-600dpi.png" dpi:DPI];
    } completionBlock:^(NSBitmapImageRep *resRep) {
        [renderManager saveBitmapRepresentation:resRep withType:NSJPEGFileType dpi:DPI quality:90.0 atPath:[NSString stringWithFormat:@"/Users/olegsehelin/Desktop/Spread2_%d.jpg",DPI]];
    }];

    [renderManager beginDrawingWithContextSize:[CropingUtilities exportedSize:exportSize withDpi:DPI] drawingBlock:^{
        [textRender drawImagesForSpreadWithText:@"The quick brown fox jumps over the lazy dog" dpi:DPI];
    } completionBlock:^(NSBitmapImageRep *resRep) {
        [renderManager saveBitmapRepresentation:resRep withType:NSJPEGFileType dpi:DPI quality:90.0 atPath:[NSString stringWithFormat:@"/Users/olegsehelin/Desktop/Spread3_%d.jpg",DPI]];
    }];

    [renderManager beginDrawingWithContextSize:[CropingUtilities exportedSize:exportSize withDpi:DPI] drawingBlock:^{
        [imagesRender drawImagesForSpreadWithImageName:@"AraAndrew_034.jpg" dpi:DPI];
    } completionBlock:^(NSBitmapImageRep *resRep) {
        [renderManager saveBitmapRepresentation:resRep withType:NSJPEGFileType dpi:DPI quality:90.0 atPath:[NSString stringWithFormat:@"/Users/olegsehelin/Desktop/Spread4_%d.jpg",DPI]];
     }];
    
    
    exportSize = NSMakeSize(10, 10);
    [renderManager beginDrawingWithContextSize:[CropingUtilities exportedSize:exportSize withDpi:DPI] drawingBlock:^{
        [imagesRender drawFirstPageForDpi:DPI];
        [textRender drawImagesForFirstSpreadWithText:@"The quick brown fox jumps over the lazy dog" dpi:DPI];
    } completionBlock:^(NSBitmapImageRep *resRep) {
        [renderManager saveBitmapRepresentation:resRep withType:NSJPEGFileType dpi:DPI quality:90.0 atPath:[NSString stringWithFormat:@"/Users/olegsehelin/Desktop/Spread1_%d.jpg",DPI]];
    }];
}

@end
