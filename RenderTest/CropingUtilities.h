//
//  CropingUtilities.h
//  Smart Albums
//
//  Created by Ivan Podibka on 9/17/13.
//
//

@class ImageResource;

@interface CropingUtilities : NSObject

/**
 Will return rotated image with given angle, angle in radians
 */
+ (NSImage *)rotatedImage:(NSImage *)anOriginalImage
				withAngle:(CGFloat)anAngle;

/**
 Returns rotated frame with specific angle
 */
+ (NSRect)rotatedFrame:(NSRect)aFrame
             withAngle:(CGFloat)anAngle
         rotationPoint:(NSPoint)aRotationPoint;

+ (NSRect)roudedRect:(NSRect)aRect;

+ (NSSize)exportedSize:(NSSize)aSize
               withDpi:(NSInteger)aDpi;
@end
