//
//  RTTextRender.h
//  RenderTest
//
//  Created by Ivan Solomichev Vakoms on 9/19/14.
//  Copyright (c) 2014 VanijaDev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RTTextRender : NSObject

- (void)drawImagesForSpreadWithText:(NSString *)aString dpi:(NSInteger)aDPI;
- (void)drawImagesForFirstSpreadWithText:(NSString *)aString dpi:(NSInteger)aDPI;

@end
